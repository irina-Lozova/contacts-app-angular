# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

# test task for XOResearch #
SPA: Angular2+, Bootstrap
view components: left sidebar, main view
navigation: left sidebar
navigation routes: contacts, groups, canvas page

Data
Take data (users and groups) from JSON-file.

Contatcs page
user { name, surname, age, group, description, note }
actions: create, edit, delete, select for action, multiselect for delete (optional) 
main view: list {name, surname}, action buttons (bottom)
detailed view (right sidebar): contact card

Groups page
group {name, members(user list)}
actions: create, edit, delete, select,multiselect (optional)
main view: list [Collapsible List Group] {name, members count [Badges]}
detailed view (on expand) :  users list {name, surname}

Canvas page
View components: 2 buttons (start, reset), Canvas field (300*300px)
Draw object on canvas using mouse events.
Move object to the right canvas border by press Start Button. 
Clear canvas using Reset Button. 

### How do I get set up? ###

1. clone repo;
2. npm i;
3. ng serve;
