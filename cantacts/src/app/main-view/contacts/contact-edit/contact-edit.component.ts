import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../../../shared/user.model';
import {ContactsService} from '../../contacts.service';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  @Output() userUnselected = new EventEmitter();
  @Output() newUserSelected = new EventEmitter<User>();
  @ViewChild('f') userForm: NgForm;
  user: User;
  subscription: Subscription;
  editMode = false;

  constructor(private contactsService: ContactsService) {
  }

  ngOnInit() {
    this.subscription = this.contactsService.startedEditing.subscribe((user: User) => {
      this.user = user;
      if (this.user) {
        this.editMode = true;
        this.userForm.setValue({
          name: this.user.name,
          surname: this.user.surname,
          age: this.user.age,
          group: this.user.group,
          description: this.user.description,
          note: this.user.note
        });
      }else {
        this.editMode = false;
      }

    });
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    if (this.editMode) {
      const changedUser = new User(this.user.id,
        value.name,
        value.surname,
        value.age,
        value.group,
        value.description,
        value.note);
      this.contactsService.saveUser(changedUser);
      this.newUserSelected.emit(changedUser);
    } else {
      const newUser = new User(Math.floor(Math.random() * 10000),
        value.name,
        value.surname,
        value.age,
        value.group,
        value.description,
        value.note);
      this.contactsService.saveUser(newUser);
    }
    this.editMode = false;
    form.reset();
  }

  onClear() {
    this.userForm.reset();
    this.editMode = false;
  }

  onDelete() {
    this.contactsService.deleteUser(this.user);
    this.userUnselected.emit();
    this.userForm.reset();
    this.editMode = false;
  }
}
