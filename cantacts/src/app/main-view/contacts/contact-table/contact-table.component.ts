import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../shared/user.model';
import {ContactsService} from "../../contacts.service";

@Component({
  selector: 'app-contact-table',
  templateUrl: './contact-table.component.html',
  styleUrls: ['./contact-table.component.css']
})
export class ContactTableComponent implements OnInit {
  @Input() users: User[];
  @Output() userSelected = new EventEmitter<User>();

  constructor(private contactsService: ContactsService) {
  }

  ngOnInit() {
  }

  onSelected(user: User) {
    this.userSelected.emit(user);
    console.log(user);
  }

  onEdit(user: User) {
    this.contactsService.startedEditing.next(user);
  }
}
