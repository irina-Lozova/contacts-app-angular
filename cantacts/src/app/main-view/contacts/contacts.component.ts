import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from '../../shared/user.model';
import {ContactsService} from '../contacts.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  users: User[] = [];

  selectedUser: User;
  constructor( private contactsService: ContactsService) {
    this.contactsService.getContacts()
    .subscribe(data => {
        this.users = data;
      console.log(this.users);
      });
  }
  ngOnInit() {}
}
