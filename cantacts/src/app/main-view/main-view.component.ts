import { Component, OnInit } from '@angular/core';
import {ContactsService} from './contacts.service';
import {GroupsService} from './groups.service';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css'],
  providers: [ ContactsService, GroupsService ]
})
export class MainViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
