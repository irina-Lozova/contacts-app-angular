import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {Group} from './groups/group.model';
import {Groups} from './groups/groups.model';
import {User} from '../shared/user.model';


@Injectable()
export class GroupsService {
  private groups: Group[] = [];
  private contactsUrl = './assets/groups.json';
  private groupsUpdater = new BehaviorSubject<Group[]>( this.groups);
  // Inject HttpClient into your component or service.
  constructor(private http: HttpClient) {
    this.http.get<Groups>(this.contactsUrl).subscribe(data => {
      // Read the result field from the JSON response.
      this.groups = data['groups'];
      this.groupsUpdater.next(this.groups);
    });
  }

  getGroups(): Observable<Group[]> {
    // Make the HTTP request:
    return this.groupsUpdater;
  }

  addMember(group: Group, member: number) {
    for (let i = 0, len = this.groups.length; i < len; i++) {
      if (group.id === this.groups[i].id) {
        this.groups[i].members.push(member);
        this.groupsUpdater.next(this.groups);
        return;
      }
    }
  }

  removeMember(group: Group, member: number) {
    for (let i = 0, len = this.groups.length; i < len; i++) {
      if (group.id === this.groups[i].id) {
        for (let j = 0, leng = this.groups[i].members.length; j < leng; j++) {
          if (member === this.groups[i].members[j]) {
            this.groups[i].members.splice(j, 1);
            this.groupsUpdater.next(this.groups);
            return;
          }
        }
      }
    }
  }

  editGroup(group: Group) {
    for (let i = 0, len = this.groups.length; i < len; i++) {
      if (group.id === this.groups[i].id) {
        this.groups[i].name = group.name;
        this.groupsUpdater.next(this.groups);
        return;
      }
    }
    this.groups.push(group);
    this.groupsUpdater.next(this.groups);
  }

  deleteGroup(group: Group) {
    for (let i = 0, len = this.groups.length; i < len; i++) {
      if (group.id === this.groups[i].id) {
        this.groups.splice(i, 1);
        this.groupsUpdater.next(this.groups);
        return;
      }
    }
  }
}
