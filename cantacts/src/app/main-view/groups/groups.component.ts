import {Component, OnInit, ViewChild} from '@angular/core';
import {GroupsService} from '../groups.service';
import {Group} from './group.model';
import {ContactsService} from '../contacts.service';
import {User} from '../../shared/user.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  @ViewChild('f') groupForm: NgForm;
  groups: Group[];
  users: User[] = [];
  newGroupMode = false;
  editMode = false;
  editGroup: Group;

  constructor(private groupsService: GroupsService, private contactsService: ContactsService) {
    this.groupsService.getGroups()
      .subscribe(data => {
        this.groups = data;
        console.log(this.groups);
      });
    this.contactsService.getContacts()
      .subscribe(data => {
        this.users = data;
      });
  }

  ngOnInit() {
  }

  filterUsers() {
    if (this.editGroup) {
      return this.users.filter((el) => {
        for (let i = 0, len = this.editGroup.members.length; i < len; i++) {
          if (el.id === this.editGroup.members[i]) {
            return false;
          }
        }
        return true;
      });
    }
    return [];
  }

  getUserById(id: number) {
    const result = this.users.filter((el) => {
      return (el.id === id);
    });
    if (result.length) {
      return result[0];
    } else {
      return null;
    }
  }

  onSelectGroup(group: Group) {
    this.editGroup = group;
  }

  onEditGroup(group: Group) {
    this.editMode = true;
    this.editGroup = group;

  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const changedGroup = new Group(this.editGroup.id, value.name, this.editGroup.members);
    if (this.editMode) {
      this.groupsService.editGroup(changedGroup);
    }
    this.editMode = false;
    form.reset();
  }

  onClear() {
    this.groupForm.reset();
    this.editMode = false;
  }

  onDelete() {
    this.groupsService.deleteGroup(this.editGroup);
    this.groupForm.reset();
    this.editMode = false;
    this.editGroup = null;
  }

  onAddNewGroup() {
    this.editMode = true;
    this.newGroupMode = true;
    this.editGroup = new Group(Math.floor(Math.random() * 10000), '', []);
  }

  onAddNewMember(member: User) {
    if (!this.newGroupMode) {
      this.groupsService.addMember(this.editGroup, member.id);
    } else {
      this.editGroup.members.push(member.id);
    }
  }

  onDeleteMember(group: Group, member: number) {
    this.groupsService.removeMember(group, member);
  }
}
