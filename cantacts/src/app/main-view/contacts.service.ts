import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Users} from '../shared/users.model';
import {User} from '../shared/user.model';
import {BehaviorSubject, Subject} from 'rxjs';


@Injectable()
export class ContactsService {
  private users: User[] = [];
  private contactsUrl = './assets/users.json';
  private userUpdater = new BehaviorSubject<User[]>(this.users);
  startedEditing = new BehaviorSubject<User>(null);
  // Inject HttpClient into your component or service.
  constructor(private http: HttpClient) {
    this.http.get<Users>(this.contactsUrl).subscribe(data => {
      // Read the result field from the JSON response.
      this.users = data['users'];
      this.userUpdater.next(this.users);
    });
  }

  getContacts(): Observable<User[]> {
    // Make the HTTP request:
    return this.userUpdater;
  }

  saveUser(user: User) {
    for (let i = 0, len = this.users.length; i < len; i++) {
      if ( user.id === this.users[i].id) {
        this.users[i] = user;
        this.userUpdater.next(this.users);
        return;
      }
    }
    this.users.push(user);
    this.userUpdater.next(this.users);
  }
  deleteUser(user: User) {
    for (let i = 0, len = this.users.length; i < len; i++) {
      if (user.id === this.users[i].id) {
        this.users.splice(i, 1);
        this.userUpdater.next(this.users);
        return;
      }
    }
  }
}
