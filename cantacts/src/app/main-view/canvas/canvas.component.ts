import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements AfterViewInit {

  @ViewChild('canvas') public canvas: ElementRef;

  @Input() public width = 400;
  @Input() public height = 400;
  private translateX: number = 0;
  private moveAnimation: number = -1;
  private leftmostPosition: number = 10000;

  private cx: CanvasRenderingContext2D;

  public ngAfterViewInit() {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.cx = canvasEl.getContext('2d');

    canvasEl.width = this.width;
    canvasEl.height = this.height;

    this.cx.lineWidth = 3;
    this.cx.lineCap = 'round';
    this.cx.strokeStyle = '#000';

    this.captureEvents(canvasEl);
  }

  private captureEvents(canvasEl: HTMLCanvasElement) {
    Observable
      .fromEvent(canvasEl, 'mousedown')
      .switchMap((e) => {
        return Observable
          .fromEvent(canvasEl, 'mousemove')
          .takeUntil(Observable.fromEvent(canvasEl, 'mouseup'))
          .takeUntil(Observable.fromEvent(canvasEl, 'mouseleave'))
          .pairwise();
      })
      .subscribe((res: [MouseEvent, MouseEvent]) => {
        const rect = canvasEl.getBoundingClientRect();

        const prevPos = {
          x: res[0].clientX - rect.left,
          y: res[0].clientY - rect.top
        };

        const currentPos = {
          x: res[1].clientX - rect.left,
          y: res[1].clientY - rect.top
        };

        this.drawOnCanvas(prevPos, currentPos);
      });
  }

  private drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }) {
    if (!this.cx) {
      return;
    }

    // this.cx.translate(this.translateX, 0);

    this.cx.beginPath();

    if (prevPos) {
      this.cx.moveTo(prevPos.x, prevPos.y); // from
      this.cx.lineTo(currentPos.x, currentPos.y);
      this.cx.stroke();
    }
  }

  private cleanCanvas() {
    this.cx.clearRect(0, 0, this.width, this.height);
  }

  private moveCanvas() {
    if (this.moveAnimation >= 0) {
      return;
    }
    const sourceImageData = this.canvas.nativeElement.toDataURL('image/png');
    const destinationImage = new Image;
    const self = this;
    destinationImage.onload = function(){
      self.moveAnimation = setInterval(() => {
        self.translateX += 10;
        self.cleanCanvas();
        self.cx.drawImage(destinationImage, self.translateX, 0);
        if (self.translateX > self.width) {
          clearInterval(self.moveAnimation);
          self.moveAnimation = -1;
          self.translateX = 0;
        }
      }, 10);
    };
    destinationImage.src = sourceImageData;
  }

  onClear() {
    this.cleanCanvas();
  }

  onMove() {
    this.moveCanvas();
  }
}
