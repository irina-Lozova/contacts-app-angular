import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { MainViewComponent } from './main-view/main-view.component';
import { AppRoutingModule } from './app-routing.module';
import { ContactsComponent } from './main-view/contacts/contacts.component';
import { GroupsComponent } from './main-view/groups/groups.component';
import { CanvasComponent } from './main-view/canvas/canvas.component';
import {HttpClientModule} from '@angular/common/http';
import { ContactDetailsComponent } from './main-view/contacts/contact-details/contact-details.component';
import { ContactTableComponent } from './main-view/contacts/contact-table/contact-table.component';
import {FormsModule} from '@angular/forms';
import { ContactEditComponent } from './main-view/contacts/contact-edit/contact-edit.component';



@NgModule({
  declarations: [
    AppComponent,
    LeftSidebarComponent,
    MainViewComponent,
    ContactsComponent,
    GroupsComponent,
    CanvasComponent,
    ContactDetailsComponent,
    ContactTableComponent,
    ContactEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
