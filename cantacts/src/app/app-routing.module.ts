import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactsComponent} from './main-view/contacts/contacts.component';
import {GroupsComponent} from './main-view/groups/groups.component';
import {CanvasComponent} from './main-view/canvas/canvas.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/contacts', pathMatch: 'full'},
  {path: 'contacts', component: ContactsComponent},
  {path: 'groups', component: GroupsComponent},
  {path: 'canvas', component: CanvasComponent}
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
